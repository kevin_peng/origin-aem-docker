#!/bin/sh

#Setup SSH tunnel port forwarding to SAP and F5
sshpass -p "Fa3BkEWBc^yk" ssh -o StrictHostKeyChecking=no -L 2443:203.20.195.251:443 -N -f dev_user@192.168.47.109
sshpass -p "Fa3BkEWBc^yk" ssh -o StrictHostKeyChecking=no -L 8050:10.215.131.25:8050 -N -f dev_user@192.168.47.109

cd /aem
java -Dhttp.proxyHost=localhost -Dhttp.proxyPort=4503 -Denv=dev -debug -XX:MaxPermSize=512M -XX:-UseSplitVerifier -Xnoagent -Xmx2048M -Xmx2048M -Djava.compiler=NONE -Dsling.run.modes=local,publish,nosamplecontent,crx3 -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=30304 -jar aem6-publish-p4503.jar -nofork -gui -verbose