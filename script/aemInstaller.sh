#!/bin/bash
while [[ $# > 1 ]]
do
    key="$1"

    case $key in
        -i|--install_file)
        FILENAME="$2"
        shift
        ;;
        -r|--runmode)
        RUNMODE="$2"
        shift
        ;;
        -p|--port)
        PORT="$2"
        shift
        ;;
        *)

        ;;
    esac
    shift
done

java -Dhttp.proxyHost=localhost -Dhttp.proxyPort=$PORT -Denv=dev -debug -XX:MaxPermSize=512M -XX:-UseSplitVerifier -Xnoagent -Xmx2048M -Xmx2048M -Djava.compiler=NONE -Dsling.run.modes=local,$RUNMODE,nosamplecontent,crx3 -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=30304 -jar $FILENAME  -listener-port 50007 -nofork -gui 2>&1 &
aemPID=$!

nc -vklp 50007 > "install.log" 2>&1 &
pid=$!

while sleep 60
do
    echo "We arr working hard to install AEM and extra packages, please wait for another 60 secs..."
    if grep --quiet "started" "install.log"
    then
        if [ -f /aem/postInstallHook.sh ] ; then { echo "running post install hook"; /aem/postInstallHook.sh; } else { echo "no post install hook found"; } fi
        kill $pid
        kill $aemPID
        exit 0
    fi
done
