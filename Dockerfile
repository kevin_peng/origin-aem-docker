FROM java:7

MAINTAINER kevin.peng@originenergy.com.au

#ENV http_proxy=${HTTP_PROXY}
RUN apt-get update && apt-get install netcat -y
#RUN apt-get install expect -y
RUN apt-get install sshpass -y

# Install utility for AEM
ADD /script/aemInstaller.sh /aem/aemInstaller.sh
RUN chmod +x /aem/aemInstaller.sh
ADD /script/postInstallHook.sh /aem/postInstallHook.sh
RUN chmod +x /aem/postInstallHook.sh
ADD /script/aemStart.sh /aem/aemStart.sh
RUN chmod +x /aem/aemStart.sh

#Setup SSH tunnel port forwarding to SAP and F5
#ADD ssh_tunnel.sh /aem/ssh_tunnel.sh
#RUN chmod +x /aem/ssh_tunnel.sh
#RUN /aem/ssh_tunnel.sh

#Copies required build media
ADD aem6-publish-p4503.jar /aem/aem6-publish-p4503.jar
ADD license.properties /aem/license.properties

#Unpack AEM
WORKDIR /aem
RUN java -jar aem6-publish-p4503.jar -unpack -v

#uploads extra packages
RUN mkdir -p /aem/crx-quickstart/install
ADD packages/*.zip /aem/crx-quickstart/install/

#config SSL
RUN mkdir /aem/crx-quickstart/ssl
RUN keytool -genkeypair -keyalg RSA -validity 3650 -alias cqse -keystore /aem/crx-quickstart/ssl/cqkeystore.keystore -keypass password -storepass password -dname "CN=$userName, OU=OriginEnergy, O=OriginEnergy, L=Melbourne, ST=Vic, C=01"

# Installs AEM
WORKDIR /aem
RUN ["/aem/aemInstaller.sh","-i","aem6-publish-p4503.jar","-r","publish","-p","4503"]

EXPOSE 4503 8000 4443 30304 8765 8050 2443

ENTRYPOINT ["/aem/aemStart.sh"]
