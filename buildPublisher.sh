#!/usr/bin/env bash

[ ! -f aem6-publish-p4503.jar ] && curl -XGET -o aem6-publish-p4503.jar  http://10.82.15.58/artifactory/software-bundles/aemSetup/aem6/author/aem6-author-p4502.jar

cd packages

[ ! -f acs-aem-commons-content-2.6.4.zip ] && curl -XGET -O http://10.82.15.58/artifactory/software-bundles/aemSetup/aem6/extra-pkgs/acs-aem-commons-content-2.6.4.zip
[ ! -f aem-service-pkg-6.0.SP3.zip ] && curl -XGET -O http://10.82.15.58/artifactory/software-bundles/aemSetup/aem6/extra-pkgs/aem-service-pkg-6.0.SP3.zip
[ ! -f aem-service-pkg-bundles-6.0.SP3.zip ] && curl -XGET -O http://10.82.15.58/artifactory/software-bundles/aemSetup/aem6/extra-pkgs/aem-service-pkg-bundles-6.0.SP3.zip
[ ! -f com.adobe.acs.bundles.twitter4j-content-1.0.0.zip ] && curl -XGET -O http://10.82.15.58/artifactory/software-bundles/aemSetup/aem6/extra-pkgs/com.adobe.acs.bundles.twitter4j-content-1.0.0.zip
[ ! -f local-ssl-config-baseInstall.zip ] && curl -XGET -O http://10.82.15.58/artifactory/software-bundles/aemSetup/aem6/extra-pkgs/local-ssl-config-baseInstall.zip

cd ..
docker build  --no-cache -t origin-aem-publisher --build-arg http_proxy=http://docker.for.mac.localhost:3128 --build-arg https_proxy=http://docker.for.mac.localhost:3128 .

#docker build  -t origin-aem-publisher .