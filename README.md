# Origin-docker-aem-publisher

This Docker image installs Adobe Experience Manager 6.x, with SSL been configured at 4443 and SSH tunnel port forwarding to SAP and F5 included.

## Prerequisite
* At this stage the instance has only been verified on MAC 
* Origin proxy cntlm configured, network ready
* Docker installed


## Usage

```
sh buildPublisher.sh

```

This process will take 10-30 minutes depend on your machine, when it stuck on messages "We arr working hard to install AEM and extra packages, please wait for another 60 secs..."


DONT stop the process, the docker building process HASNT finished yet,
JUST WAIT!!!


Once building finished, you can verify and run your image by

```bash
docker images
sh runPublisher.sh
```

This will take around 20 secs, please check server running at

http://localhost:4503/crx/packmgr/index.jsp

https://origin-local.adobecqms.net:4443 

should also be accessible(although it's blank page for now, because you still have to deploy origin-cms)

## Others

* Any additional packages `.zip` can be manually placed in /packages folder, and require re-run buildPublisher.sh
